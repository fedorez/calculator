//
//  ViewController.h
//  Calculator
//
//  Created by Denis Fedorets on 07.11.16.
//  Copyright © 2016 Denis Fedorets. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalculatorEngine.h"

@interface ViewController : UIViewController {
    IBOutlet UILabel *display;
    CalculatorEngine *calcEngine;
    double calculatorMemory;
    BOOL userIsInTheMiddleOfTypingANumber;
    BOOL decimalAlreadyEnteredInDisplay;
}

- (IBAction)digitPressed: (UIButton *)sender;
- (IBAction)operationPressed: (UIButton *)sender;
- (IBAction)decimalPressed: (UIButton *)sender;
- (IBAction)clearCalculator;
- (IBAction)deleteDigit;

- (IBAction)memoryActionPressed: (UIButton *)sender;

@end

