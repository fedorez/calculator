//
//  AppDelegate.h
//  Calculator
//
//  Created by Denis Fedorets on 07.11.16.
//  Copyright © 2016 Denis Fedorets. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

