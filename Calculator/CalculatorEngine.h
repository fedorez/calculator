//
//  CalculatorBrain.h
//  Calculator
//
//  Created by Denis Fedorets on 09.11.16.
//  Copyright © 2016 Denis Fedorets. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CalculatorEngine : NSObject {
    double operand;
    double waitingOperand;
    NSString * waitingOperation;
}

- (void)setOperand:(double)aDouble;
- (double)performOperation:(NSString *)operation;
- (void)performWaitingOperation;

@end
